# Anticensura

## Dal niente da nascondere al niente da mostrare: sviluppare insieme pratiche più sicure in Internet

***Julie Gommes***

![cipolle](../../es/content/media/circumvention-tools.png)	

Mi piace molto quando la gente mi dice non ha niente da nascondere: "Quindi posso farti un video dentro la doccia?", sguardo esterefatto. Ma no! "Oppure, posso farti un video quando russi la notte? O almeno lasciami leggere la tua cartella medica....Ah, no?hai qualcosa da nascondere?" 

Ci saranno sempre aspetti della nostra vita che vogliamo mantenere intimi, per timidezza, paura, o semplicemente per il piacere di avere un giardino segreto, un mondo nostro. In più, se non hai niente da nascondere allora nessuna persona vorrà confidarti un segreto. È problematico. Come fare dunque per avere amici? Questa idea di **trasparenza radicale**[^1] che promuovono i difensori del web sociale-commerciale è una trappola per le nostre libertà individuali. 

Tanto più quando questo sforzo di trasparenza sembra non applicarsi ai nostri "rappresentanti" polici, né alle imprese. Quindi, perché la **cittadinanza** dovrebbe esporsi di forma continua per provare che non ha niente da nascondere?

La creazione attiva di spazi sicuri non può lasciare da parte le tecnologie digitali ed Internet. La sicurezza deve pensarsi come un congiunto di pratiche che ingloba le nostre identità fisiche ed elettroniche, le due faccie della stessa moneta. Se la sicurezza può interpretarsi come l'assenza di rischi o come la fiducia in persone o cose, deve essere interpretata anche come un processo multidimensionale. Questa visione significa saper proteggere il tuo corpo (del quale solo tu decidi!), il tuo diritto ad esprimerti, alla cooperazione, all'anonimato, ma anche il tuo diritto ad imparare dagli strumenti e dalle applicazioni che ti difendono. Per questo bisogna capire che alternative esistono e come si possono usare, difendere e appoggiare.

La percezione di sicurezza dipende da come ci connettiamo, navighiamo e scambiamo informarmazioni, ma anche del tipo di tecnologia che usiamo e con chi la usiamo. I sistemi operativi, il tipo di hardware, gli XISP, i server, i router contano. Le nostre finalità sociali e politiche influiscono nel tipo di sicurezza di cui avremo bisogno e come tenteremo scoprire, coprire o esporre le nostre traccie. A volte cercheremo l'anonimato, la autenticità, la prova di integrità delle comunicazioni, la cifratura dei contenuti, altre volte cercheremo tutte queste dimensioni assieme.

Senza dubbio, il paradosso della **privacy** ci insegna che le persone generalmente hanno la tendenza ad affermare che si preoccupano per la loro intimità, ma quando gli si chiede che misure utilizzano per proteggerla, ci si rende rapidamente conto che non ne prendono nessuna, o quasi. Al principio di Internet esisteva l'idea che potevamo stare li e adottare qualunque identità[^2], come descriveva Steiner nel 1993: **"On the Internet, nobody knows you're a dog[^3]"**. Al giorno d'oggi questa epoca di internet è finita. Adesso ci etichettano, ci profilano, ci monitorizzano, ci analizzano.

Siamo quello che il nostro grafico sociale[^4] dice di noi, e coloro che non sviluppano pratiche per difendersi si incontrano totalmente senza protezione. Nude e nudi in Internet:"Si però ok...la sicurezza è difficile".

O no, neanche tanto. Se prendi un tempo minimo per interessarti al tema, il tempo di riscrivere la tua password per impedire che si possa accedere ai tuoi dati se ti rubano il computer o lo smartphone, il tempo di alzare la testa per controllare se c'è una videocamera che guarda sulla tua tastiera. Il tempo di formulare le buone domande come, per esempio, a che rischi si è esposte e come prevenirli. O anche domandare come le tue pratiche on line espongono la vita privata delle tue amicizie o del collettivo con il quale vuoi cambiare il mondo.

Migliorare le proprie pratiche in Internet significa anche avere più libertà nelle proprie opinioni, e poterle esprimere con sicurezza. Più libertà di lavorare come giornalista, per esempio. Mi fa arrabbiare quando leggo "intervista realizzata su skype" con persone che possono morire per quella che io chiamo negligenza. Come giornalista, ed al di là di tutta la mia buona volontà ed i molti sforzi, sbagliavo anche io, per ignoranza. Oggi mi sorprende quando la persona con cui sto parlando non sa cos'è la Deep Packet Inspection[^5], però, a dir la verità, neanche io lo sapevo fino ad un paio di anni fa. Perciò lo spieghiamo, lo ripetiamo una ed un'altra volta. Perchè prendersi il tempo per spiegare queste nozioni e strumenti a persone del proprio intorno -ma non solo- è un contributo fondamentale per promuovere un Internet ed una società più giuste per tutte e tutti. 

Imparare a proteggersi ed a non mettere le altre persone in pericolo ha bisogno di tempo ed attenzione, però conferisce automatismi che saranno benefici nel quotidiano.

### Presa di coscienza
Al giorno d'oggi non si può ignorare lo spionaggio on line. Che si tratti delle rivelazioni di Edward Snowden rispetto alla NSA o delle detenzioni ripetute di chi si opponeva, prima e dopo le rivoluzioni del 2011, non possiamo più ignorare che potenzialmente potremmo essere tutte sotto vigilaza. Questa situazione si verifica anche **offline**, con la videovigilanza. Se sono in una grande via di negozi con delle amiche, ci sarà sicuramente una videoregistrazione di quel momento anche se la mia immagine, il mio sorriso, un momento di intimità o confidenza che non hanno niente a che fare in un dabase. È la mia vita.

## Sdrammatizzare

La protezione della vita privata non è riservata ad un'elite appassionata alla tecnica, e passa molte volte attraverso piccoli gesti quotidiani e, prima di tutto, per una presa di posizione. Chiunque ha rilevato, (sopratutto) me inclusa, pezzi della nostra vita nel web, per una mancante conoscenza delle consequenze. Chiunque ha parlato della vita privata di persone amiche, prima di rendersi conto del danno che stava causando. Probabilmente abbiamo caricato foto nostre, perchè avevamo travestimenti fighi, perchè eravamo felici, perchè ci amavamo e non pensavamo che questi momenti sarebbero finito nell'ufficio di un agenzia di marketing o in un dossier dei servizi segreti.

### Scegliere

Non siamo apostoli del fare bene, del vivere meglio, nè le messaggiere della sacra protezione di dati. Vogliamo solo, con la tecnica che conosciamo, arricchita dagli errori commessi, darvi alcuni consigli basici per aiutarvi a proteggervi, o per lo meno, farvi riflettere su quello che (non) dovreste insegnare. Presto ci si renderà conto che bisognerà scegliere tra comodità e libertà, però, come diceva Benjamin Franklin "Un popolo pronto a sacrificare poca della sua libertà in cambio di poca sicurezza non merità né una cosa nell'altra, e finisce per perdere entrambe."
Quindi al lavoro! Per scappare dalla vigilanza in maniera semplice e senza dolore, bisogna solo rimpiazzare i vostri strumenti quotidiani con strumenti sicuri. PrismBreak[^6], non importa il sistema operativo usato (si, anche windows, anche se ne sconsigliamo vivamente l'uso n.d.t.), propone strumenti che permettono schivare la vigilanza elettronica. E per evitare la videovigilanza il progetto "sotto vigilanza"[^7], permette consultare le mappe delle città dove ci si trova: Minsk, Mosca, Seattle, Parigi, etc, e così darsi appuntamento con le proprie fonti, amicizie, gruppi di azione, dove non ci sono videocamere e quindi evitare il pesante sguardo del Grande Fratello.

### Dell'importanza della riappropiazione degli strumenti
A ciascuna pratica/persona/necessità corrisponte uno strumento. Non ci si anonimizza nella stessa maniera se si vuole recuperare materiale didattico come docente o se si è un'adolescente che vuole scaricare la musica preferita. Interessarsi per il proprio computer, per capire come funziona è anche capire che non c'è una soluzione miracolosa o uno strumento rivoluzionario. Iteressarsi vuol dire anche domandarsi quali sono i programmi che possono essere malevoli. Per esempio, perché un'applicazione di disegno in uno smartphone chiede i permessi per avere accesso alla mia rubrica o al mio archivio di SMS? Perché un'applicazione di note ha bisogno di localizzarmi? Possiamo renderci conto molto facilmente di come i creatori di alcune applicazioni si danno permessi sui nostri dispositivi. Bisogna solamente leggere le caratteristiche prima di fare click su "Installa". Un'altra volta non si hanno bisogno di competenze tecniche per proteggersi, solamente curiosità verso gli strumenti che si usano.

### Disciplina
Possiamo imparare a lanciare e usare questo o quel software, creare partizioni crittate con Truecrypt[^8], però se non siamo coscienti dei rischi che facciamo correre alle altre persone quando le chiamiamo al telefono o le mandiamo una email senza cifrarla, la tecnologia non serve a niente. Oltre il difficile apprendimento degli strumenti, è una disciplina che bisogna imparare, essere coscienti di quello che facciamo o di quello che non facciamo e delle conseguenze che possono portare. È una presa di coscienza quotidiana. È importante creare momenti di apprendimento collettivo, momenti di scambio, per poter pensare la sicurezza in una rete personale dove anche le amicizie e i parenti adottano queste pratiche per creare un circolo virtuoso dove ognuni persona stimola le altre. Scambiarsi email cifrate, scegliere un'indirizzo email che non dipenda da un'impresa commerciale, o lavorare insieme a tutorial e manuali sono buone pratiche di appoggio mutuo.

### Anonimato, perché? Come?
Oltre le soluzioni tecniche, l'anonimato e l'uso di pseudonimi possono constituire soluzioni semplici alla vigilanza. L'uso di pseudonimi è mostrare un'altra identità in Internet, che sia di corta o lunga durata, che serva per una chat di alcuni minuti o per identificarsi in un forum nel quale si parteciperà per anni. L'anonimato è non lasciare nessuna traccia che permetta il riconoscimento. Alcuni strumenti semplici lo permettono. Tor[^9], per esempio, fa compiere dei salti da un server ad un altro nel momento stesso della connessione al sito. Il risultato? È l'indirizzo IP di uno dei server e non il vostro che verra salvato nei registri della vostra connessione.

### Crittare, un gioco da ragazze
Inviare una mail "in chiaro" è lo stesso che inviare una cartolina. Il postino la può leggere nel cammino, vedere la foto, può scherzarci su, etc. La vostra cartolina viaggia senza protezione né contro la pioggia né contro occhiate indiscrete. Con le vostre email succede lo stesso. Tranne se, come nel sistema di posta, si mette il messaggio in una busta. La busta digitale si ottiene crittando.
Quando ero bambina, lo facevamo in scala ridotta inviandoci messaggi segreti con le amiche. Allo scegliere un codice tipo "saltare di tre lettere", "Ti voglio bene" si trasforma in "zo brlonr ehqh ". Crescendo non diventa molto più complicato. La differenza è che i computer lavorano per noi e fanno sì che crittare sia ancora più complesso, più difficile da rompere, con caratteri speciali, algoritmi che crittano un messaggio senza nessuna corrispondenza con il prossimo che critteranno.

### Della servitù volontarie
Nel caso delle e-mail, quando facciamo click su "inviare" il messaggio questo viene imagazzinato in quattro copie:
1. Il primo nella cartella di invio di chi la manda, si trova facilmente andando sulla cartella "posta inviata".
2. Il secondo, nella cartella in entrata di chi la riceve. Fino ad ora niente di anormale, tranne che...
3. La terza copia viene imagazzinata in un server del signore Google, della signora Yahoo, dell'impresa della email di chi invia. Bisogna aggiungere che chiunque abbia accesso a questi server, che lavori o meno per questa compagnia, può avere accesso a queste email.
4. E non finisce qui, visto che la quarta copia la conserva la signora Google, il signor Yahoo, l'impresa della email di chi riceve. Quindi, ancora una volta, chiunque abbia accesso a questi server, che lavori o meno per questa compagnia, può avere accesso a queste email.

Cancellare i messaggi dalla cartella in arrivo o di uscita dell'interfaccia non li cancella dai server, li stanno imagazzinati e li rimangono. Anche se tutto questo risulta detestabile rispetto la vita privata, siamo noi che lo permettiamo.

### Conclusioni
Proteggere la propria vita privata, quella delle persone che si relazionano a noi, delle nostre amicizie, non si improvvisa, però non è una sfida insuperabile. A volte basta riflettere prima di cliccare, prima di installare un'applicazione. Il resto è solo tecnica e sta alla portata di tutto il mondo, basta solo volerla apprendere.

### Alcune guide e tutorial per iniziare


**Security in a box**: una guida che spiega che strumenti usare a seconda della situazione concreta. Esiste in 13 lingue: https://securityinabox.org/

**How to bypass Internet censorship**: La spiegazione passo passo dell'installazione della maggior parte degli strumenti di sicurezza, attraverso screenshot esplicativi. Esiste in 9 lingue: http://howtobypassinternetcensorship.org/

**Prism Break**: proteggersi sul cellulare e sul computer sostituendo i prori strumenti con strumenti sicuri: https://prism-break.org/

**Cryptocat**: un software di chat sicuro attraverso il proprio navigatore:  https://crypto.cat/


---

**Julie Gommes**
Analista in cybersicurezza e giornalista che scrive codice e parla con il suo computer con linee di comandi. Ha vissuto e lavorato in Medio Oriente e nel sud-est asiatico. Partecipa in diversi collettivi per difendere la neutralità della rete e lottare contro la società della vigilanza.

Il suo blog in francese: http://seteici.ondule.fr
jujusete[at]riseup[point]net
PGP D7484F3C e @jujusete su twitter.


---


### NOTE
[^1]:	 http://www.ippolita.net/fr/libro/la-confidentialit%C3%A9-n%E2%80%99est-plus-l%E2%80%99id%C3%A9ologie-de-la-transparence-radicale

[^2]:	 https://upload.wikimedia.org/wikipedia/en/f/f8/Internet_dog.jpg

[^3]:	 https://en.wikipedia.org/wiki/On_the_Internet,_nobody_knows_you%27re_a_dog

[^4]:	 http://es.wikipedia.org/wiki/Grafo_social

[^5]:	 https://es.wikipedia.org/wiki/Inspección_profunda_de_paquete

[^6]:	 https://prism-break.org/en/

[^7]:	 www.sous-surveillance.net

[^8]:	 http://www.truecrypt.org/

[^9]:	 https://www.torproject.org/

<p align="center"><img src="../../end0.png"></p>