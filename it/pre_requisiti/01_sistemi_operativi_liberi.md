# Il software libero è ancor più necessario che mai

## Richard Stallman

![gnu e pinguino](../../es/content/media/operating-systems.png)

*Una versione ridotta di questo articolo è stata pubblicata su Wired [^1]*

--------


Sono passati ormai 30 anni dalla nascita del movimento per il software libero, il cui obiettivo è quello di promuovere software che tuteli la libertà dell'utenza e della comunità.
Questo software viene denominato «libero» (in riferimento alla libertà e non al prezzo [^2]. 
Alcuini programmi proprietari come Photoshop sono eccessivamente costosi, altri come Flash sono gratuiti; a prescindere, in entrambi i casi l'utente viene sottomesso al controllo del produttore del software.

Parecchie cose sono cambiate da quando abbiamo cominciato.
Nei paesi sviluppati del mondo odierno chiunque possiede un computer (a volte chiamati «telefonini») con cui collegarsi a Internet.
Il software proprietario continua imponendo il suo controllo attraverso funzioni informatiche ad utenti completamente ignari di esse. Inoltre ora è possibile imporre tale controllo anche attraverso nuovi modelli _SaaSS_ (Service as a Software Substitute), ovvero «Servizio Surrogato del Software»  che permette a un computer esterno di eseguire funzioni sul tuo dispositivo.

Sia il software proprietario che i SaaSS, possono spiare, incatenare e perfino attaccare i loro utenti.
I frequenti abusi nei servizi e prodotti del software proprietario sono possibili proprio perché gli utenti non hanno nessun controllo.
Infatti, questa è la differenza fondamentale: tanto il software proprietario come i SaaSS sono sotto il controllo esclusivo (spesso di aziende o dello Stato).
Mentre il software libero, al contrario, offre in mano a tutti i suoi utenti questo potere.

### Il Controllo

Perché il controllo è così importante? Perché la libertà implica poter assumere il controllo della propria vita.
Impiegando un software per svolgere delle attività attinenti alla tua vita, la tua libertà dipenderà esclusivamente dal controllo che puoi esercitare su di esso.
Meriti d'avere tutto il controllo sui programmi che utilizzi, soprattutto se vengono impiegati per realizzare dei compiti a te indispensabili.

Affinchè l'utenza possa assumere il controllo dei programmi impiegati sono indispensabili quattro **libertà essenziali.** [^3]

0. Libertà di eseguire il programma come si desidera, per qualsiasi scopo.

1. Libertà di studiare il codice sorgente del programma e di modificarlo in modo da adattarlo alle proprie necessità.
I programmatori scrivono i programmi in un determinato linguaggio di programmazione (qualcosa di simile all'inglese combinato con l'algebra), cosa che viene denominata «codice sorgente».
Chiunque sia in grado di programmare e abbia a sua disposizione il codice sorgente del programma può leggere tale codice, capire i suoi meccanismi e dunque modificarlo.
Invece, quando si ha unicamente a disposizione il programma come eseguibile binario (cioè, una lista di numeri eseguibili dal computer, ma difficilmente decifrabile per noi umani), capire il programma e modificarlo a piacere diventa un compito al quanto complesso.

2. Libertà di ridistribuire copie del programma quando lo desideri. Questo non è un obbligo è una possibilitá.
Che il programma sia libero non implica che una persona debba fare delle copie oppure debbano farti delle copie.
Mentre distribuire programmi senza le libertà essenziali è un affronto verso chi l'utilizza. Chiaramente se non si copiano e se ne fa un uso semplicamente privato questo rispetta la libertá e non rappresenta un danno per nessuna persona.

3. Libertà di migliorare il programma e distribuirne pubblicamente i miglioramenti apportati.

Le due prime libertà garantiscono ad ogni utente la possibilità di controllare individualmente il programma.
Le altre due invece, permettono a un qualunque gruppo di utenti di **controllare collettivivamente** l'uso del software.
L'intero insieme delle quattro libertà permette che tutti gli insieme si assuma il controllo completo sul programma.
Se una di queste libertà va a mancare o viene modificata il programma non è libero, è proprietario e quindi ingiusto.

In vari ambiti pratici vengono impiegate anche opere di vario genere, tipo ricette di cucina; materiale didattico (libri di testo, manuali, dizionari ed enciclopedie); tipi di caratteri; diagrammi circuitali per la produzione hardware; oppure stampanti 3D per la manifattura di oggetti funzionali (non necessariamente ornamentali).
Anche se tutte queste ed altre opere non appartengono alla categoria del software, il movimento del software libero tenta di accogliere questo vasto insieme nello stesso modo, cioè applicando su di esse lo stesso ragionamento di base e arrivando la stessa conclusione: tutte queste opere devono garantire le quattro libertà essenziali.

Il software libero permette di sperimentare ed aggiungere modifiche a un determinato programma, così da fargli realizzare quel che a noi serve (oppure eliminare ciò noi non interessa).
Modificare il software può risultare fuori luogo per chi è abituato alle scatole chiuse del software proprietario, ma nel mondo del software libero è consuetudine, inoltre rappresenta un ottimo modo per imparare a programmare.
Persino il passatempo di aggiustare le proprie macchine, usanza comune in vari paesi, viene ostacolato dal fatto che ora le vetture portano all'interno del software proprietario.


### L'ingiustizia del privativo

Se le persone non controllano il programma, è il software a controllarle.

Nel caso del software proprietario, esiste sempre un'entità (il «proprietario» del programma) che controlla il programma ed e' proprio attraverso il programma che usa il suo potere e decide come e chi lo usa.
Un programma che non è libero è dunque oppressore, uno strumento di potere ingiusto.

Nei casi estremi (ormai diventati quasi normali), i programmi proprietari sono progettati per spiare, limitare, censurare o abusare di chi lo usa [^4].
Cosa che vien fatta, ad esempio, dal sistema operativo degli i-cosi [^5] di Apple e anche dai dispositivi mobili Windows con processori ARM.
I firmware dei telefonini, il navigatore Google Chrome per Windows e lo stesso sistema operativo, includono una backdoor universale che permette a una certa azienda di modificare programmi in modalità remota senza bisogno di permessi.
Il Kindle di Amazon permette la cancellazione dei libri tramite un'apposita backdoor.

Col proposito di finire con l'ingiustizia del software proprietario, il movimento per il software libero sviluppa programmi liberi così da garantire  agli utenti le proprie libertà.
Movimento che nasce con lo sviluppo del sistema operativo libero GNU nel 1984.
Ad oggi, milioni di computer funzionano con **GNU** [^6] , principalmente con la combinazione **GNU/Linux** [^7].

Distribuire programmi senza concederne le dovute libertà, implica un maltrattamento e un abuso per chi usa quei programmi.
La non distribuzione di un programma, invece, non genera danni per nessuna persona.
Certo, se scrivi un software e lo usi solo per te non stati facendo male, al massimo sprechi la possibilità di _fare del bene_ favorirendo qualche altra persona che ne avrebbe bisogno, cosa che però è ben diversa da _far del male_.
Quindi, quando affermiamo che tutto il software devovrebbe essere libero, intendiamo che tutte le copie di un programma messe a disposizione ad altri dovrebbero concedere le quattro libertà essenziali, ma non che tutti coloro che sviluppano programmi debbano concedere obbligatoriamente delle copie in giro ad altre persone.


### Software proprietario e SaaSS

Il software proprietario è stato il primo mezzo impiegato dalle aziende per prendersi il controllo dei compiti informatici delle persone. Oggi tale controllo viene spesso imposto attraverso i servizi chiamati SaaSS (_Service as a Software Substitute_), tradotto in italiano sarebbe «Servizio come Surrogato del Software», ovvero un servizio che permette a un computer esterno di eseguire alcuni  compiti sui quali non abbiamo assolutamente il controllo.

Anche se di solito i programmi all'interno dei server fornitori di SaaSS sono proprietari, tali server possono anche includere o essere interamente progettati utilizzando software libero.
Purtroppo, l'utilizzo in sé dei SaaSS provoca le stesse ingiustizie inerenti all'utilizzo del software proprietario: sono due percorsi che portano alla stessa cattiva fine.
Si consideri, per esempio, un SaaSS per le traduzioni: l'utente invia una o varie frasi al server, il server traduce (per esempio dall'inglese all'italiano) e poi reinvia il testo tradotto all'utente.
In questo modo, il compito di traduzione è sotto il controllo dell'amministratore del server e non dell'utente.

Se usi un servizio SaaSS, chi controlla il server controlla anche i tuoi compiti informatici.
Questo implica che affidi tutti i dati necessari all'amministratore del server, il che può essere costretto un giorno a fornire tale informazioni allo Stato o a terzi; quindi una domanda sorge spontanea: **chi è che sta servendo veramente questo servizio?** [^8]


### Ingiustizie primarie e secondarie

Impiegando programmi privati o SaaSS si provoca un danno, poiché concedi ad altri di esercitare un potere ingusto su di te.
Per il proprio bene si dovrebbe evitarne l'uso.
Se l'utente acconsente di "non condividere" si danneggiano anche gli altri.
Accettare questo tipo di compromesso è un male, smettere è meno peggio, ma la cosa giusta è non iniziare affatto.

Ci sono situazioni in cui l'uso del software proprietario incoraggia altre persone a farne uso.
Skype è un chiaro esempio: se qualcuno impiega il loro client, forza altre persone a farne uso a discapito delle proprie libertà.
Anche Google Hangouts presenta lo stesso problema.
È del tutto sbagliato proporre software di questo genere.
Infatti, dobbiamo rifiutarci di utilizzare questi programmi, anche per pochi istanti, perfino nei computer di altri individui.

Riassumendo, si può dire che l'impiego di programmi privativi e SaaSS consente di premiare l'oppressore, farne propaganda, promuovendone lo sviluppo di tale programma o «servizio» e spingendo sempre più persone a sottostare al dominio di un unica azienda proprietaria.
Nel caso un cui l'utente corrisponde a un ente pubblico o una scuola, i danni indiretti *in tutte le sue forme* raggiungono una dimensione maggiore.


### Il software libero e lo Stato


Gli enti pubblici sono stati concepiti per i cittadini, e non come istituzioni indipendenti.
Per tanto, tutti i compiti informatici che svolgono, li svolgono facendo le veci dei cittadini e delle cittadine.
Hanno dunque il dovere di mantenere il totale controllo su tali compiti ai fini di garantire la loro corretta esecuzione.
È questa, infatti, la sovranità informatica dello Stato.
Esattamente per questo che nessun ente dovrebbe mai permettere che il controllo dei compiti informatici dello Stato venga delegati e sovlti da imprese private.
Per avere un totale controllo delle mansioni informatiche svolte a favore dei cittadini, gli enti pubblici non dovrebbero impiegare software proprietario (software sottoposto al controllo di entità non statali).

Delegare lo svolgimento di tali compiti a un servizio esterno programmato ed eseguito da entità non interne allo Stato sarebbe ugualmente sbagliato, poiché in questo modo farebbero uso di SaaSS e non avrebbero il controllo di ciò che succede.
Il software proprietario non offre protezione alcuna contro una minaccia fondamentale: il suo sviluppatore, che potrebbe facilitare terzi a portare a compimento un attacco.
Prima di correggere gli errori di Windows, Microsoft li fornisce a alla NSA, la agenzia di spionaggio digitale del governo degli Stati Uniti [^9].
Non abbiamo conoscenza se Apple fa lo stesso, ma è sempre sotto la stessa pressione statale di Microsoft. 


### Software libero e l'educazione

Le scuole (e tutte le istituzioni educative a sua volta) influiscono sul futuro della società tramite i loro insegnamenti.
Nel campo informatico, per garantire che tale influenza sia positiva, le scuole dovrebbero insegnare unicamente software libero.
Insegnare l'uso di un programma proprietario equivale ad imporne la dipendenza, azione del tutto contraria alla missione educativa.
Incentivando l'uso tra gli studenti di software libero, le scuole reindirizzano il futuro della società verso la libertà, e aiuteranno alla formazione di programmatori di talento.

Inoltre, così facendo, le scuole trasmetterebbero agli studenti l'abitudine di cooperare ed aiutare agli altri.
Le scuole, cominciando dalle elementari, dovrebbero dire agli studenti: *«Cari studenti, questo è un luogo dove si condivide il sapere. Se porti a scuola del software devi dividerlo con gli altri bambini. Devi mostrare il codice sorgente ai compagni, se qualcuno vuole imparare. Quindi è vietato portare a scuola software proprietario se non per studiare come funziona ai fini di poterlo riprodurre».*

Seguendo gli interessi degli sviluppatori di software proprietario, gli studenti non potrebbero né acquisire l'abitudine di condividere il software né  sviluppare le capacità per modificarli, qualora ci fosse la curiosità ed interesse tra loro.
Cosa che implica una mala formazione accademica.
Nella sezione **GNU Education** [^10] è possibile trovare informazione dettagliate sull'uso di software libero nelle scuole e nelle istituzioni educative.


### Software libero: molto più che «vantaggioso»

Molto speso mi viene chiesto di descrivere i «vantaggi» associati al software libero.
Il termine «vantaggi» però è del tutto insignificante quando si parla di libertà.
La vita senza libertà è tirannia, cosa che si applica tanto alla informatica come a qualunque altra attività attinente alla nostra vita. Dobbiamo rifiutarci di concedere il controllo dei nostri compiti informatici ai proprietari di programmi o servizi informatici.
È quel che deve essere fatto, per ragioni egoistiche o no

La libertà implica la possibilità di cooperare con altri.
Negare tale libertà è significa voler dividere le persone, che porta unicamente ad opprimerle.
Nella comunità del software libero siamo molto consci dell'importanza della libertà di cooperare, appunto perché il nostro lavoro corrisponde a una cooperazione organizzata.
Se un qualche conoscente viene a trovarti e ti vede usare un programma, questa persona potrebbe chiederti una copia.
Qualunque programma che impedisce la sua libera distribuzione, o ti impone l'obbligo di *non cooperare*, è antisociale.

In informatica, la cooperazione implica la distribuzione della stessa copia di un programma fra diversi utenti.
Ma a sua volta, implica anche la distribuzione delle sue versioni modificate.
Il software libero promuove queste forme di cooperazione, mentre quello proprietario le vieta.
Anche il SaaSS impedisce di cooperare: se deleghi i tuoi compiti informatici a un servizio web custodito nel cloud o in un server esterno, mediante una copia di un programma altrui, non puoi nè vedere nè toccare il software usato, di conseguenza non puoi né distribuirlo liberamente né modificarlo.

### Conclusioni

Tutti noi meritiamo di avere il controllo della nostra vita informatica.
Come possiamo ottenerlo? Rifiutandoci di impiegare software proprietario nei nostri computer oppure quelli di uso frequente, e anche, rifiutando i servizi SaaSS **Sviluppando software libero** [^11] per chi lavora nel campo della programmazione; rifiutando di sviluppare o promuovere software proprietario o SaaSS e **diffondendo queste idee** [^12].

Noi, e altri migliaia di utenti, continuiamo a farlo sin dal 1984, e grazie a questo sforzo oggi abbiamo il sistema operativo libero GNU/Linux, che tutti: da chi lo sviluppa e chi lo usa, possono modificare, distribuire e utilizzare.
Unitevi alla nostra causa, ben sia come programmatore oppure attivista.
Facciamo in modo che tutti gli utenti di computer siano liberi!


----
**Richard Matthew Stallman**

Programmatore statunitense e fondatore del movimento per il software libero nel mondo.
Tra i suoi meriti di programmazione all'interno del progetto GNU spiccano la realizzazione dell'editore di testo Emacs, il compilatore GCC e il debugger GDB.
È principalmente conosciuto per lo sviluppo del quadro di riferimento morale, politico e legale all'interno del movimento del software libero, come alternativa di sviluppo e distribuzione al software proprietario.
È stato anche l'inventore del concetto di CopyLeft (anche se non del termine), metodo per concedere in licenza il software garantendo che tanto il suo utilizzo, come ulteriori modifiche, rimangano libere e sempre a portata della comunità di utenti e sviluppatori.


----

### NOTE

[^1]: http://www.wired.com/opinion/2013/09/why-free-software-is-more-important-now-than-ever-before

[^2]: In inglese, il termine «free» può significare «libero» oppure «gratuito».

[^3]: https://gnu.org/philosophy/free-sw.html

[^4]: https://gnu.org/philosophy/proprietary.html

[^5]: Presso da «iThings», termine dispregiativo per fare riferimento ad aggeggi tipo iPod, iPad, iPhone, ecc.

[^6]: http://gnu.org/gnu/the-gnu-project.html

[^7]: http://gnu.org/gnu/gnu-linux-faq.html

[^8]: https://gnu.org/philosophy/who-does-that-server-really-serve.html

[^9]: www.arstechnica.com/security/2013/06/nsa-gets-early-access-to-ze-ro-day-data-from-microsoft-others 

[^10]: http://www.gnu.org/education

[^11]: https://gnu.org/licenses/license-recommendations.html

[^12]: https://gnu.org/help

<p align="center"><img src="../../end0.png"></p>